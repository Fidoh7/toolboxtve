'use strict';

let app = require('../app');
let chai = require('chai');
let request = require('supertest');
let expect = chai.expect;

describe('Message Test', function() {
    describe('#GET / message', function() {
        it('should get message', function(done) {
            request(app).get('/api/message?message=test')
                .end(function(err, res) {
                    expect(res.statusCode).to.equal(200);
                    expect(res.body).to.be.an('string');
                    expect(res.body).to.be.equal('test');
                    done();
                });
        });

        it('should get error', function(done) {
            request(app).get('/api/message?message')
                .end(function(err, res) {
                    expect(res.statusCode).to.equal(500);
                    expect(res.body).to.be.eql({ message: 'Not Message Provided' });
                    done();
                });
        });
    });
});
