'use strict'

let express = require('express');
let MessageController = require('../controllers/message');

let api = express.Router();

api.get('/message', MessageController.getMessage);

module.exports = api;
