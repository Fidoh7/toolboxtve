'use strict';

let config = require('./config');
let express = require('express');
let bodyParser = require('body-parser');

let app = express();

let messagRouter = require('./routes/message');

app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

app.use('/api', messagRouter);

app.listen(config.port, function () {
    console.log(`App listening on port ${config.port}!`);
});

module.exports = app;
