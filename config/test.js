'use strict';

module.exports = {
    env: 'test',
    port: process.env.PORT || 3100,
};
