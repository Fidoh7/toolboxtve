'use strict';

function getMessage(req, res) {
    const message = req.query.message;

    if (message == null || message === '') {
        res.status(500).send({message: 'Not Message Provided'});
    }

    res.json(message);
}

module.exports = {
    getMessage
};
