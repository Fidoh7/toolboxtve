# README #

### Que hay en este repositorio? ###

Test enviado por Toolbox TVE a Leandro Martin Quiñonez.

### Dependencias necesarias para ejecutar la aplicación? ###

1. Docker
2. Docker-compose
3. Node 8

### Como ejecutar la aplicación? ###

1. npm install
2. docker-compose up

### Como ejecutar el script cliente? ###

1. node client.js "example text"

### Como ejecutar los tests de la aplicación? ###

1. npm install
2. npm test
