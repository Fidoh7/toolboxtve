'use strict';

let rp = require('request-promise');

const message = process.argv[2];
const URL = 'http://localhost:3000/api/message';

if (message == null || message === '') {
    console.error('Not Message Provided');
    process.exit(1);
}

var options = {
    uri: `${URL}?message=${message}`,
    json: true
};

rp(options)
    .then(function (response) {
        console.log(response);
        process.exit(0);
    })
    .catch(function (err) {
        console.error(err);
        process.exit(1);
    });


